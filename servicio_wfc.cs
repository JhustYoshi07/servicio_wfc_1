﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace servicio_wfc
{
    
    //Jhustin Ismael Arias Perez 4-A T/M

    [ServiceContract]
    public interface persona_wfc
    {
        [OperationContract]
        requisito dato_usuario(string dato_guardado);
    }

    [DataContract]
    public class requisito: mensaje
    {//DataMember: forma parte de un contrato de datos  y se puede serializar
        [DataMember]
        public string nombre_usuario { get; set; }
        [DataMember]
        public int edad_usuario { get; set; }
       [DataMember]
        public string genero_usuario { get; set; }
       
    }

    [DataContract]
    public class mensaje
    {
        [DataMember]
        public string dato_obtenido { get; set; }
        
    }
}
